Some examples from the work of Godfried Toussaint.

(2,3) West Africa
(3,5) Cumbia
(3,5) Romania
(5,6) Arab
(4,7) Bulgaria
(5,7) Arab
(3,8) Tresillo
(5,8) Cinquillo
(7,8) Tuareg rhythm of Libya
(4,9) Turkey
(5,9) Arab
(4,11) Frank Zappa
(5,12) South Africa
(7,12) Ghana and Guinea
(5,16) Bossa Nova
(7,16) Samba
(9,16) Brazil, West and Central Africa
(11,24) Central Africa
(13,24) Central Africa
