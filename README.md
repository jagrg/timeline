# timeline

This is a timeline-based metronome using a non-Western approach to
time-keeping and based on the works of James Koetting and Godfried
Toussant.

[Live demo](https://jagrg.gitlab.io/timeline/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
